#!/bin/bash
#
# List all processes (pids) of a subhost

SUBPID=( $(pgrep -f "gnt-start $1") )

if [ -z "$SUBPID" ] ; then
    pgrep -af gnt-start >&2
    exit 1
fi

expand() {
    local IFS=,
    for P in "$@" $(ps -hopid --ppid="${*}") ; do echo $P ; done | sort -nu
}

N=0
while [ $N != ${#SUBPID[@]} ] ; do
    N=${#SUBPID[@]}
    SUBPID=( $(expand "${SUBPID[@]}") )
    echo "${SUBPID[*]}"
done
