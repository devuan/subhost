#!/bin/bash
#
# This is a helper script for reviewing all mount namespaces to find
# a given mount line

pids() {
    ls -l /proc/*/ns/mnt | tr / ' ' | \
	awk '{print $10,$NF}' | sort -uk2,2 | sed 's/ .*//'
}

for p in $(pids) ; do
    nsenter -t $p -m grep / /proc/mounts >/dev/null 2>&1 || \
	nsenter -t $p -m mount -t proc proc /proc 2> /dev/null || \
	continue
    nsenter -t $p -m grep / /proc/mounts >/dev/null 2>&1 || continue
    if nsenter -t $p -m grep "$1" /proc/mounts ; then
	ps -hopid,cmd $p
	nsenter -t $p -m
    fi
done
