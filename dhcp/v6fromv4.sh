#!/bin/bash

if [ $# != 2 ]; then
	echo "Usage:   $0 IPv6-prefix IPv4-address"
	echo "Example: $0 2a01:4f8:140:32a1::/96 100.64.200.202"
	exit 1
fi

pfx=$(echo $1|tr / ' '|awk '{print $1}')
msk=$(echo $1|tr / ' '|awk '{print $2}')

printf '%s%02x%02x:%02x%02x/%s\n' $pfx $(echo $2|tr . ' ') $msk
