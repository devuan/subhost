#!/bin/bash

# Require template and configuration files
if [ ! -f dhcpd-template.conf ] ; then
	echo Config file dhcpd-template.conf does not exist >&2
	exit 1
fi

if [ ! -f public.conf ] ; then
	echo Config file public.conf does not exist >&2
	exit 1
fi

[ ! -d generated ] && if ! mkdir generated; then
	echo Can not create generated folder >&2
	exit 1
fi

# Function to make the hosts table.
hosts() {
	local H Egnt Agnt Elan Alan Hid pubs pub6
	local TABLE="--no-headers -o name,nic.mac/0,nic.ip/0,nic.mac/1,nic.ip/1"
	gnt-instance list $TABLE | while read H Egnt Agnt Elan Alan; do
		H="${H%%..*}"
		Hid=$(echo $H|tr . ' '|awk '{print $1}')
		pubs=$(grep -E "^4[ \t]+$Hid[ \t]+" public.conf|awk '{print $3}')
		pubs=$(echo -n $pubs|tr ' ' ,)
		if [ ! -z "$pubs" ]; then
			pubs="option pubips $pubs; "
		fi
		pub6=$(grep -E "^6[ \t]+$Hid[ \t]+" public.conf|awk '{print $3}')
		pub6=$(echo -n $pub6|tr ' ' ,)
		if [ ! -z "$pub6" ]; then
			pubs="${pubs}option pubip6 \"G6 $pub6\"; "
		fi
		cat <<EOF
host gnt-$H { hardware ethernet $Egnt; fixed-address $Agnt; }
host lan-$H { hardware ethernet $Elan; fixed-address $Alan; $pubs}
EOF
	done
}

# Capture the hosts table for reuse in each node's DHCP configuration
hoststable="$(hosts)"

# uncomment these two for a quick debug run
#echo "$hoststable"
#exit

# Prepare the individual DHCP response configurations which are both
# set up on the nodes and saved locally
spreadconf() {
	local ip id node gw6 SETUP RESTART lhoststable
	gnt-node list --no-headers -oname,pip | while read node ip; do
		id=${ip##*.}
		node=$(echo ${node%%..*}|tr . ' '|awk '{print $1}')
		gw6=$(grep -E "^$node[ \t]+" hosts.conf | awk '{print $4}' | grep -v @ | head -n1)
		SETUP="tee /etc/dhcp/dhcpd.conf"
		RESTART="service isc-dhcp-server restart >/dev/null"
		lhoststable=$(echo "$hoststable" | sed -e s/G6/$gw6/)
		echo "Updating $node (gw $ip gw6 $gw6)"
		cat <<EOF | ssh $ip "$SETUP ; $RESTART" > generated/dhcpd-$id.conf
$(sed -e s/GW/$id/ < dhcpd-template.conf)
$lhoststable
EOF
	done
}

spreadconf
