#!/bin/bash

if [ "$#" -lt "1" ]; then
	echo Usage: $0 '[-s|--secondaries] [search1 [search2 ...]]'
	echo Examples:
	echo -e "\t$0" -s bonito.ganeti1.devuan.org$
	echo -e "\t\t - list net on VMs with secondary bonito"
	echo -e "\t$0" "' polpo'"
	echo -e "\t\t - list net on VMs on polpo"
	echo -e "\t$0" ^gal
	echo -e "\t\t - list net on VMs matching ^gal"
	echo -e "\t$0" "''"
	echo -e "\t\t - list net on all VMs"
	exit
fi

fields=-oname,pnode
search=()

for i in "$@"; do
	case $i in
		-s|--s|--se|--sec|--seco|--secon|--second|--seconda|--secondar|--secondari|--secondarie|--secondaries)
			fields=-oname,pnode,snodes
			;;
		*)
			search+=("$i")
			;;
	esac
done

greps="grep -v ^hurd"
for i in "${search[@]}"; do
	if [ "${#greps}" != "0" ]; then
		greps+='|'
	fi
	greps+="grep -E -- \"$i\""
	#echo element: "\"$i\""
done
if [ "${#greps}" = "0" ]; then
	greps=cat
fi
#echo $greps

for i in $(gnt-instance list $fields --no-header|. <(echo $greps)|tr . ' '|awk '{print $1}'); do
	echo -n $(eval echo ={1..$((50/2-${#i}/2-1))})|sed 's/[0-9]* \?//g'
	echo -n " $i "
	echo $(eval echo ={1..$((50-(50/2-${#i}/2-1)-2-${#i}))})|sed 's/[0-9]* \?//g'
	ssh $i \
		"ip a|grep inet|grep -vE 'fe80::|127[.]0[.]0[.]1|::1/128'|sed -e 's/inet6 /inet6\t/' -e 's/inet /inet4\t/'|sort;\
		ip r|grep default;\
		ip -6 r|grep default;\
		grep nameserver /etc/resolv.conf"
done
