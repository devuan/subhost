#!/bin/bash

# Require configuration files
if [ ! -f hosts.conf ] ; then
	echo Config file hosts.conf does not exist >&2
	exit 1
fi

[ ! -d generated ] && if ! mkdir generated; then
	echo Can not create generated folder >&2
	exit 1
fi

# Function to generate the policy config per host
policyrt() {
	# generate policy route setup for node $1
	echo '#!/bin/bash'
	echo '#' generate: $1
	# list all nodes (skip our node)
	local node pip pit
	grep -vE '^[ \t]*$|^[ \t]*#' hosts.conf|awk '{print $1}'|sort -u|while read node; do
		node=$(echo ${node%%..*}|tr . ' '|awk '{print $1}')
		if [ "$node" = "$1" ]; then
			continue
		fi
		# get node's internal ipv4, used as table id + 100
		pip=$(getent hosts $node.localnet.devuan.org|awk '{print $1}')
		pit=${pip##*.}
		echo '#' node: $node pip: $pip pit: $pit
		# for each other node generate policy table
		# first flush them, then fill each available entry
		echo ip route flush table 1$pit
		echo ip -6 route flush table 1$pit
		local host n4 n6 gw6
		grep -E "^$node[ \t]+" hosts.conf | while read host n4 n6 gw6; do
			echo '#' n4: $n4 n6: $n6 gw6: $gw6
			if [ ! $n4 = @ ]; then
				echo ip rule del from $n4
				echo ip rule add from $n4 table 1$pit
				echo ip route add default via $pip table 1$pit
			fi
			if [ ! $n6 = @ ]; then
				echo ip -6 rule del from $n6
				echo ip -6 rule add from $n6 table 1$pit
				echo ip -6 route flush table 1$pit
				echo ip -6 route add $gw6 dev lan_br table 1$pit
				echo ip -6 route add default via $gw6 table 1$pit
			fi
		done
		# in each policy table add direct routing for the internal subnets
		grep -vE '^[ \t]*$|^[ \t]*#' hosts.conf|while read host n4 n6 gw6; do
			if [ ! $n6 = @ ]; then
				echo ip -6 route add $n6 dev lan_br table 1$pit
			fi
		done
		# locally source the used ipv4 and also add them to each policy table
		echo "for ip in \$(/etc/network/external-ip.sh listip); do ip route add \$ip dev lan_br table 1$pit; done"
	done
}

policyrtall() {
	local node pip dtotal
	gnt-node list --no-headers -o name,pip,dtotal | while read node pip dtotal; do
		echo Got $node $pip $dtotal
		node=$(echo ${node%%..*}|tr . ' '|awk '{print $1}')
		pip=${pip##*.}
		# skip offline nodes
		if [ $dtotal = '?' ]; then
			echo Skip offline node $node
			continue
		fi
		echo Generate policy routes for $node
		rtfile="$(policyrt $node)"
		echo "$rtfile" > generated/policy-routes-$pip.sh
		chmod +x generated/policy-routes-$pip.sh
		echo Copy rules to $node via scp
		scp generated/policy-routes-$pip.sh $node:/etc/network/policy-routes.sh
		echo Apply rules to $node via ssh
		ssh $node /etc/network/policy-routes.sh < /dev/null
	done
}

policyrtall
