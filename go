#!/bin/bash

# list of nodes
nodes() {
    ssh polpo gnt-node list --no-header -oname | sed 's|\..*||'
}

# filter the unshare command line into "pid name"
filter_unshare() {
    sed -e 's|\([0-9]*\).*chroot \([^ ]*\).*|\1\2|' \
	-e 's|\(.*/\([^/]*\)/[^/ ]*$\)|\2'@$1':\1|'
}

# enlist all subhosts on all nodes in format: $name@$node:$pid$path
subhosts() {
    for NODE in $(nodes) ; do
	ssh $NODE pgrep -a unshare | filter_unshare $NODE
    done
}

ALLSUBS="$(subhosts|sort)"
[ -z "$1" ] || SUB="$(echo "$ALLSUBS" | grep -E "^$1@")"

if [ -z "$SUB" ] ; then
    SEL=( $(echo "$ALLSUBS" | sed 's|@.*||') )
    select SUB in ${SEL[*]} ; do
	[ -z "$SUB" ] && continue
	SUB="$(echo "$ALLSUBS" | grep -E "^$SUB@")"
	break
   done
fi

echo "$SUB"

NAME="${SUB%%@*}" ; SUB="${SUB#*@}"
NODE="${SUB%%:*}" ; SUB="${SUB#*:}"
SPID="${SUB%%/*}" ; SUB="${SUB#*/}"
SPID="$(ssh $NODE ps -hopid --ppid $SPID)"
echo "NODE/SPID = $NODE/$SPID"
LIVE="/$SUB"
CMD="ip netns exec $NAME nsenter -t $SPID -m -p -i -u chroot $LIVE /bin/bash"
CMD="nsenter -t $SPID -m -p -i -u ip netns exec $NAME chroot $LIVE /bin/bash"
[ "$NODE" = "$(hostname -s)" ] || CMD="ssh -t $NODE $CMD"
echo "$CMD"
[ -z "$SPID" ] || exec $CMD
echo "NOT A SUBHOST $LIVE???"
