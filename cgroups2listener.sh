#!/bin/bash
#
if [ -z "$1" ] ; then
    {
	flock -n 9 || exit 0
	exec socat -u udp-recvfrom:519,bind=10.0.10.250,fork exec:"$0 read" &
	exit 0
    } 9> /run/lock/cgroups2listener.lock
fi

read -t 2 HOST KEY VALUE MORE || exit 0

HOSTS=( borta ns3 mini )

goodhost() {
    local H
    for H in ${HOSTS[@]} ; do
	[ "$H" = "$1" ] && return 0
    done
    return 1
}

# Number of microseconds/100 between updates
R=1800000
DB="/var/cache/rwhodsnap/loadhistories2.sqlite"
M=$(( $(date +%s) / 60 ))

sql() {
    echo "SQL: $*" >&2
    echo ".timeout 2000
$*" | sqlite3 $DB
}

# H V W
update() {
    L="$(newlisp -e "(int (div (sub $2 $3) $R))")"
    echo "update $* $L" >&2
    sql "insert or replace into measure values('$1','$2');
insert into history values('$1','$M','$L');"
}

case "$1" in
    read)
	[ -z "$MORE" ] || exit 0
	[ "$KEY" = "usage_usec" ] || exit 0
	goodhost "$HOST" || exit 0
	W="$(sql "select value from measure where host='$HOST';" )"
	update "$HOST" "$VALUE" "${W:-$VALUE}" "W=$W"
	;;
    *)
	:
	;;
esac
